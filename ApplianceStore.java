import java.util.Scanner;

public class ApplianceStore {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Oven specs = new Oven();
		Oven[] ovens = new Oven[4];
		
		for(int i = 0; i < ovens.length; i++) {
			ovens[i] = new Oven();
			
			System.out.println("Please enter the oven's model: ");
			ovens[i].model = sc.next();
			System.out.println("Please enter the oven's colour: ");
			ovens[i].colour = sc.next();
			System.out.println("Please enter the oven's maximum temperature: ");
			ovens[i].maxTemperature = sc.nextInt();
		}
		System.out.println(ovens[3].model);
		System.out.println(ovens[3].colour);
		System.out.println(ovens[3].maxTemperature);
		
		ovens[0].printModel();
		specs.isMaxTempHigh(ovens[0].maxTemperature);
	}
}