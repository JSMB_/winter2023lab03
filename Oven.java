public class Oven {
	public String model;
	public String colour;
	public int maxTemperature;
	
	public void printModel() {
		System.out.println("This oven's model is: " + model);
	}
	
	public void isMaxTempHigh(int temp) {
		if(temp < 235) {
			System.out.println("This oven's maximum temperature is normal.");
		} else {
			System.out.println("This oven's maximum temperature is above average!");
		}
	}
}